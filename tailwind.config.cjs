/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    fontFamily: {
      dmSans: ['DM Sans'],
      poppins: ['Poppins']
    },
    extend: {
      colors: {
        darkBlue: '#183b56',
        darkGrey: '#3f475c',
        grey: '#838995',
        grey001: '#5a7184',
        grey002: '#92aabd',
        grey003: '#acb8c1',
        grey004: '#c4c4c4',
        grey005: '#ecedef',
        grey006: '#e8ebee',
        grey007: '#e9edeb',
        grey008: '#e5e5e5',
        pumpkin: '#E95432',
        orange: '#FF8345',
        apricot: '#fda87c',
        plango: '#ffb583',
        bgCircleOrange: 'rgb(255 244 236)',
        bgSubscribe: '#f3f6fb',
        white: '#ffffff',
        green: '#7bc678',
        divPink: 'rgba(233, 84, 50, 0.1)'
      },
      screens: {
        mobile: '375px',
        tablet: '600px',
        laptop: '1280px',
        desktop: '1440px'
      },
    }
  },
  plugins: []
}
