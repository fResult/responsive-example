import React, { FC } from 'react'
import Logo from '../../assets/icons/Logo.svg'
import Hamburger from '../../assets/icons/Hamburger.svg'

type HeaderProps = {
  className?: string
}

const navigationUrls = [
  {
    name: 'Home',
    url: '#'
  },
  {
    name: 'Job',
    url: '#'
  },
  {
    name: 'How it works',
    url: '#'
  },
  {
    name: 'Testimonial',
    url: '#'
  }
]

const Header: FC<HeaderProps> = ({ className = '' }) => {
  return (
    <header className={`${className} px-6 py-5 laptop:px-[120px] desktop:px-[250px]`}>
      <nav className="flex justify-between items-center">
        <a href="/">
          <img src={Logo} alt="Logo Go to Home" />
        </a>

        <button className="laptop:hidden" onClick={(e) => alert('Open Menu')}>
          <img src={Hamburger} alt="Menu List Button" />
        </button>

        <ul className="mobile:hidden laptop:flex gap-x-6">
          {navigationUrls.map((navigation) => {
            return (
              <li key={navigation.name}>
                <a className="px-6 py-4 text-darkBlue whitespace-nowrap" href={navigation.url}>
                  {navigation.name}
                </a>
              </li>
            )
          })}
        </ul>

        <ul className="mobile:hidden laptop:flex gap-x-4">
          <li>
            <a className="px-11 py-6 bg-white text-darkBlue" href="#">
              Register
            </a>
          </li>
          <li>
            <a className="px-11 py-6 bg-orange text-white whitespace-nowrap" href="#">
              Sign In
            </a>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
