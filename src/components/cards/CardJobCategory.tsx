import React, { FC } from 'react'

import { IJobCategory } from '../../@types'

import IconArrowArrowRight from '../../assets/icons/arrow-right.svg'

type CardJobCategoryProps = {
  className?: string
  jobCategory: IJobCategory
}

const CardJobCategory: FC<CardJobCategoryProps> = ({
  className = '',
  jobCategory
}) => {
  return (
    <div
      className={`${className} card__job-category flex flex-col justify-between p-[30px] h-[268px] hover:bg-grey008 border border-solid border-[#ededed] hover:border-grey008 rounded-lg`}
    >
      <img
        className="w-20 h-20 rounded-full shadow-lg"
        src={jobCategory.svgIcon}
        alt=""
      />

      <div className="flex justify-between">
        <div>
          <p className="text-xl leading-8 font-semibold">{jobCategory.name}</p>
          <p className="text-base leading-7 font-normal text-grey001">
            {jobCategory.positionRemaining} jobs opened
          </p>
        </div>
        <button className="p-2 rounded-full">
          <img
            className="text-white"
            src={IconArrowArrowRight}
            alt={`See job: ${jobCategory.name}`}
          />
        </button>
      </div>
    </div>
  )
}

export default CardJobCategory
