import React, { FC, ReactNode } from 'react'

import { IJobCategory } from '../../@types'

import CardJobCategory from '../cards/CardJobCategory'

import IconArrowArrowRight from '../../assets/icons/arrow-right.svg'
import IconDigitalMarketing from '../../assets/icons/job-categories/digital-marketing.svg'
import IconArtAndDesign from '../../assets/icons/job-categories/art-n-design.svg'

type SectionJobCategoriesProps = {
  className?: string
}

const jobCategories: IJobCategory[] = [
  {
    name: 'Digital Marketing',
    url: '#',
    svgIcon: IconDigitalMarketing,
    positionRemaining: 143
  },
  {
    name: 'Art & Design',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 110
  },
  {
    name: 'Accounting',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 158
  },
  {
    name: 'Photography',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 95
  },
  {
    name: 'Copy Writing',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 113
  },
  {
    name: 'Music',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 55
  },
  {
    name: 'Photography',
    url: '#',
    svgIcon: IconArtAndDesign,
    positionRemaining: 25
  }
]

const SectionJobCategories: FC<SectionJobCategoriesProps> = ({ className = '' }) => {
  return (
    <section className={`section-job-categories ${className} py-[60px]`}>
      <h2 className="flex flex-col items-center font-bold text-[30px] leading-[40px] laptop:text-[40px] laptop:leading-[50px] mb-[30px] tablet:mb-[50px] laptop:mb-[70px]">
        <span>Browse Jobs by</span> <span>Category</span>
      </h2>
      <ul className="grid grid-cols-1 tablet:grid-cols-2 laptop:grid-cols-4 gap-6 laptop:gap-[30px]">
        {jobCategories.map((jobCategory) => {
          return (
            <li key={jobCategory.name}>
              <a href={jobCategory.url}>
                <CardJobCategory jobCategory={jobCategory} />
              </a>
            </li>
          )
        })}
        <li>
          <a href="#">
            <div
              className="card__job-category flex justify-between items-center p-[30px] h-[268px] bg-orange hover:bg-apricot group rounded-lg"
              style={{
                backgroundImage: 'url("../../assets/bg/bg-card-explomore.svg")'
              }}
            >
              <p className="text-white group-hover:text-darkGrey text-6 leading-[34px] font-medium w-[102px]">
                Explore 30 More Categories
              </p>
              <button className="p-2 rounded-full translate-y-6">
                <img src={IconArrowArrowRight} alt="Explore More Job Categories" />
              </button>
            </div>
          </a>
        </li>
      </ul>
    </section>
  )
}

export default SectionJobCategories
