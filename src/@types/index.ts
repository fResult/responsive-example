export interface IJobCategory {
  name: string
  url: string
  svgIcon: string
  positionRemaining: number
}