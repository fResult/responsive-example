import React, { FC } from 'react'
import Header from '../components/layouts/Header'
import SectionJobCategories from '../components/sections/SectionJobCategories'

type HomeProps = {
  className?: string
}

const Home: FC<HomeProps> = ({ className = '' }) => {
  const APP_PADDING_SIDE = 'px-6 laptop:px-[120px] desktop:px-[250px]'
  return (
    <div>
      <Header />

      <main>
        <section
          className={`grid place-items-center [height:calc(960px-106px)] bg-grey text-grey008 ${APP_PADDING_SIDE}`}
        >
          <div className="flex flex-col gap-y-12">
            <p className="text-[96px]">First Section</p>
            <p className="flex justify-center gap-6 text-3xl">
              Scroll down
              <button className="animate-bounce translate-x-[25%]">&darr;</button>
            </p>
          </div>
        </section>

        <SectionJobCategories className={APP_PADDING_SIDE} />
      </main>

      <footer className="px-6 laptop:px-[120px] desktop:px-[250px]">Footer</footer>
    </div>
  )
}

export default Home
